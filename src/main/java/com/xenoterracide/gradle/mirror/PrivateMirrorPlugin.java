/*
 * Copyright 2018 Caleb Cushing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xenoterracide.gradle.mirror;

import org.apache.commons.lang3.StringUtils;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.artifacts.dsl.RepositoryHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;

public class PrivateMirrorPlugin implements Plugin<Project> {
    private static final String PROP = "jar.repository.uri";
    private final Logger log = LoggerFactory.getLogger( this.getClass() );

    @Override
    public void apply( Project project ) {
        PropertyAccessor properties = new PropertyAccessor( project.getProperties() );
        String uri = properties.getProperty( PROP );
        log.info( PROP + "={}", uri );
        RepositoryHandler repoHandler = project.getRepositories();
        if ( StringUtils.isNotBlank( uri ) ) {
            repoHandler.maven( repo -> {
                repo.setUrl( URI.create( uri ) );
                String user = properties.getProperty( "jar.repository.user" );
                String pass = properties.getProperty( "jar.repository.pass" );
                if ( StringUtils.isNotBlank( user ) && StringUtils.isNotBlank( pass ) ) {
                    repo.credentials( creds -> {
                        creds.setUsername( user );
                        creds.setPassword( pass );
                    } );
                }
            } );
        }
        repoHandler.mavenCentral();
    }
}
