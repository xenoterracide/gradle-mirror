/*
 * Copyright 2019 Caleb Cushing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xenoterracide.gradle.mirror;

import java.util.Locale;
import java.util.Map;

class PropertyAccessor {
    private final Map<String, ?> properties;

    PropertyAccessor( Map<String, ?> properties ) {
        this.properties = properties;
    }

    String getProperty( String property ) {

        @SuppressWarnings( "unchecked" )
        String val = System.getenv( envName( property ) );
        if ( val == null ) {
            val = (String) properties.get( property );
        }
        return val;
    }

    static String envName( String prop ) {
        return prop.toUpperCase( Locale.ENGLISH ).replace( ".", "_" );
    }
}
