plugins {
    idea
    `java-library`
    `java-gradle-plugin`
    id("com.gradle.plugin-publish").version("0.10.0")
    id("com.xenoterracide.gradle.sem-ver").version("0.8.4")
}
group = "com.xenoterracide"
repositories {
    mavenCentral()
}

dependencies {
    implementation("org.apache.commons:commons-lang3:3.+")
}

dependencyLocking {
    lockAllConfigurations()
}
java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

pluginBundle {
    // These settings are set for the whole plugin bundle
    vcsUrl = "https://bitbucket.org/xenoterracide/gradle-mirror"
    website = vcsUrl
    plugins {
        create("mirror") {
            id = "com.xenoterracide.gradle.$name"
            displayName = "Xeno's Mirror Plugin"
            description = "publish to an s3 bucket, read from an http proxy"
            tags = listOf("repository")
        }
    }
}

